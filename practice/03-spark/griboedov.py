from pyspark import SparkContext, SparkConf

config = SparkConf().setAppName("griboedov").setMaster("yarn")
spark_context = SparkContext(conf=config)

rdd = spark_context.textFile("/data/griboedov")
rdd2 = rdd.map(lambda x: x.strip().lower())
rdd3 = rdd2.flatMap(lambda x: x.split(" "))
rdd4 = rdd3.map(lambda x: (x,1))
rdd5 = rdd4.reduceByKey(lambda a, b: a + b).sortBy(lambda a: -a[1]) 
words_count = rdd5.take(10)

for word, count in words_count:
    print word.encode("utf8"), count
